import React from 'react'
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'

import LandingPage from './components/pages/LandingPage'
import LoginPage from './components/pages/LoginPage'
import RegisterPage from './components/pages/RegisterPage'
import ForgetPasswordPage from './components/pages/ForgetPasswordPage'
import HomePage from './components/pages/HomePage'

import './App.css'

export default function App() {
    return (
        <Router basename={ process.env.PUBLIC_URL }>
            <div style={ MainStyle }>
                <Routes>
                    <Route exact path="/" Component={ LandingPage } />
                    <Route path="/login" Component={ LoginPage } />
                    <Route path="/register" Component={ RegisterPage } />
                    <Route path="/forget-password" Component={ ForgetPasswordPage } />
                    <Route path="/home" Component={ HomePage } />
                </Routes>
            </div>
        </Router>
    )
}

const MainStyle = {
    textAlign: "center",
}