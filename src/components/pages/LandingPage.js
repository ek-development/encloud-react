import React from 'react'
import { Link } from 'react-router-dom'

import '../../App.css';
import Logo from '../../assets/images/logo.png';

export default function LandingPage() {
    return (
        <header style={ HeaderStyle }>
            <img src={ Logo } alt="Logo" style={ LogoStyle }></img>
            <h1 className="main-title text-center">EnCloud</h1>
            <div className="buttons text-center">
                <Link to="/login">
                    <button className="primary-button">log in</button>
                </Link>
                <Link to="/register">
                    <button className="primary-button" id="reg_btn"><span>register </span></button>
                </Link>
            </div>
        </header>
    )
}

const HeaderStyle = {
    width: "100%",
    height: "100vh",
    textAlign: "center",
}

const LogoStyle = {
    width: "384px",
    height: "384px",
}